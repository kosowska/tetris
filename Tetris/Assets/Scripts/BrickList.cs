﻿
using System.Collections.Generic;
public class BrickInfo
{
    public List<KeyValuePair<int, int>> brickBitmap;
    public int bitmapHeight;
    public int bitmapWidth;

    public BrickInfo(List<KeyValuePair<int, int>> brickBitmap, int bitmapHeight, int bitmapWidth)
    {
        this.brickBitmap = brickBitmap;
        this.bitmapHeight = bitmapHeight;
        this.bitmapWidth = bitmapWidth;
    }
}

public static class BrickList
{
    public static string[] BrickNames = 
    {
        "LTetromino",
        "ITetromino",
        "SquareTetromino",
        "TTetromino",
        "SkewTetromino",
        "ZTetromino",
        "JTetromino"
    };
    private static BrickInfo LTetromino = new BrickInfo(
        new List<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(2, 1)
        }, 3, 3);
    private static BrickInfo JTetromino = new BrickInfo(
        new List<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(2, 0),
            new KeyValuePair<int, int>(2, 1)
        }, 3, 3);
    private static BrickInfo ITetromino = new BrickInfo(
        new List<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(2, 1),
            new KeyValuePair<int, int>(3, 1)
        }, 4, 4);
    private static BrickInfo SquareTetromino = new BrickInfo(
        new List<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1)
        }, 2, 2);
    private static BrickInfo TTetromino = new BrickInfo(
        new List<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(0, 2),
            new KeyValuePair<int, int>(1, 1)
        }, 3, 3);
    private static BrickInfo SkewTetromino = new BrickInfo(
        new List<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(0, 2),
            new KeyValuePair<int, int>(1, 0),
            new KeyValuePair<int, int>(1, 1)
        }, 3, 3);
    private static BrickInfo ZTetromino = new BrickInfo(
        new List<KeyValuePair<int, int>>()
        {
            new KeyValuePair<int, int>(0, 0),
            new KeyValuePair<int, int>(0, 1),
            new KeyValuePair<int, int>(1, 1),
            new KeyValuePair<int, int>(1, 2)
        }, 3, 3);
    public static Dictionary<string, BrickInfo> Bricks = new Dictionary<string, BrickInfo>()
    {
        {"LTetromino", LTetromino},
        {"ITetromino", ITetromino},
        {"SquareTetromino", SquareTetromino},
        {"TTetromino", TTetromino},
        {"SkewTetromino", SkewTetromino},
        {"ZTetromino", ZTetromino},
        {"JTetromino", JTetromino}
    };

}
