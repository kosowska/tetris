﻿using System.Collections.Generic;
using UnityEngine;

public class MainBoardViewModel
{
    [SerializeField] 
    private Transform brickStartPosition;
    [SerializeField] 
    private int boardWidth, boardHeight;
    private Color[,] boardState;
    private int score;
    private int level = 1;
    private int pointsScale = 100;
    public int BoardHeight => boardState.GetLength(0);
    public int BoardWidth => boardState.GetLength(1);
    public bool IsFieldFilled(int i, int j) => boardState[i, j] != Color.white;
    public Color GetFieldColor(int i, int j) => boardState[i, j];
    public BrickViewModel brickViewModel;
    public KeyValuePair<int,int> startBrickPosition;
    public KeyValuePair<int,int> currentBrickPosition;
    public KeyValuePair<int,int> previousBrickPosition;
    private int[] nextLevelThresholds = {200, 500, 1000, 1500, 2500, 3500};
    public int GetScore => score;
    public MainBoardViewModel(int boardHeight, int boardWidth)
    {
        boardState = new Color[boardHeight, boardWidth];
        for (int i = 0; i < BoardHeight; i++)
        {
            for (int j = 0; j < boardWidth; j++)
            {
                boardState[i,j] = Color.white;
            }
        }
        startBrickPosition = new KeyValuePair<int,int>(0, (BoardWidth / 2 - 2));
        SetUpStartPosition();
    }

    public void AddToScore(int points)
    {
        score += points * pointsScale;
    }

    public int GetLevel => level;
    public int NextLevelThreshold => nextLevelThresholds[level-1];

    public void IncreaseLevel()
    {
        level++;
    }
    public bool IsPositionValid(bool[,] bitmap, KeyValuePair<int,int> position)
    {
        int height = bitmap.GetLength(0);
        int width = bitmap.GetLength(1);
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (bitmap[i,j])
                {
                    if (position.Key + i < 0 || position.Value + j < 0 || 
                        position.Key + i >= BoardHeight || position.Value + j >= BoardWidth)
                    {
                        return false;
                    }
                    if (IsFieldFilled(position.Key + i, position.Value + j))
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }
    public void MoveNewBrick(KeyValuePair<int,int> direction)
    {
        KeyValuePair<int, int> newPosition = new KeyValuePair<int, int>(currentBrickPosition.Key + direction.Key,
            currentBrickPosition.Value + direction.Value);
        if (IsPositionValid(brickViewModel.Bitmap, newPosition))
        {
            currentBrickPosition = newPosition;
        }

        previousBrickPosition = currentBrickPosition;
    }
    public void RotateNewBrick()
    {
        bool[,] rotatedBitmap = brickViewModel.GetRotatedBitmap();

        if (IsPositionValid(rotatedBitmap, currentBrickPosition))
        {
            brickViewModel.RotateBrick();
        }
    }
    public void SetUpStartPosition()
    {
        previousBrickPosition = startBrickPosition;
        currentBrickPosition = startBrickPosition;
    }

    public void SaveBrickToBoard()
    {
        for (int i = 0; i < brickViewModel.BitmapHeight; i++)
        {
            for (int j = 0; j < brickViewModel.BitmapWidth; j++)
            {
                if (brickViewModel.IsFieldFilled(i, j))
                {
                    boardState[i + currentBrickPosition.Key, j + currentBrickPosition.Value] =
                        brickViewModel.BrickColor;
                }
            }
        }
    }

    public List<int> GetFullLinesIndx()
    {
        List<int> indexes = new List<int>();
        for (int i = 0; i < BoardHeight; i++)
        {
            int numOfFilled = 0;
            for (int j = 0; j < BoardWidth; j++)
            {
                if (IsFieldFilled(i, j)) numOfFilled++;
            }

            if (numOfFilled == BoardWidth) indexes.Add(i);
        }

        return indexes;
    }

    public void DeleteFullLines(List<int> indxOfFullLines)
    {
        for (int i = 0; i < indxOfFullLines.Count; i++)
        {
            int indx = indxOfFullLines[i];
            for (int j = 0; j < BoardWidth; j++)
            {
                boardState[indx, j] = Color.white;
            }
        }
    }

    public void MovePartsAfterDeleting(List<int> indxOfFullLines)
    {
        for (int i = 0; i < indxOfFullLines.Count; i++)
        {
            for (int j = indxOfFullLines[i] - 1; j >= 0; j--)
            {
                for (int k = 0; k < BoardWidth; k++)
                {
                    if( j + 1 > BoardHeight) continue;
                    boardState[j + 1, k] = boardState[j,k];
                }
            }
        }
    }
}
