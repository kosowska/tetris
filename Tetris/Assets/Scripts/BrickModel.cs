﻿
public class BrickModel
{
    public bool[,] bitmap;

    public BrickModel(int indx)
    {
        string brickName = BrickList.BrickNames[indx];
        BrickInfo brickInfo = BrickList.Bricks[brickName];
        bitmap = new bool[brickInfo.bitmapHeight, brickInfo.bitmapWidth];
        for (int i = 0; i < brickInfo.brickBitmap.Count; i++)
        {
            bitmap[brickInfo.brickBitmap[i].Key, brickInfo.brickBitmap[i].Value] = true;
        }
    }

}
