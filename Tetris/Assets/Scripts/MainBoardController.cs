﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBoardController : MonoBehaviour
{
    private MainBoardViewModel boardModel;
    private MainBoardView boardView;
    private bool isBrickBlocked;
    private float lastMoveTime;
    private KeyValuePair<int,int> previousBrickPosition;
    private KeySystem keySystem;
    private bool isGameOver;
    [SerializeField] 
    private float fullLinesPauseTime = 0.2f;
    [SerializeField] 
    private float moveTime = 1.3f;
    public NextBrickBoardView nextBrickBoardView;
    private NextBrickBoardViewModel nextBrickModel;

    private void CheckKeyboard(out bool wasDownPressed)
    {
        wasDownPressed = false;
        if (keySystem.GetKeyDown(KeyCode.LeftArrow))
        {
            boardModel.MoveNewBrick(new KeyValuePair<int, int>(0, -1));
            boardView.DrawNewBrick(boardModel);
        }
        else if (keySystem.GetKeyDown(KeyCode.RightArrow))
        {
            boardModel.MoveNewBrick(new KeyValuePair<int, int>(0, 1));
            boardView.DrawNewBrick(boardModel);
        }
        else if (keySystem.GetKeyDown(KeyCode.UpArrow))
        {
            boardModel.RotateNewBrick();
            boardView.DrawNewBrick(boardModel);
        }
        else if (keySystem.GetKeyDown(KeyCode.DownArrow))
        {
            wasDownPressed = true;
        }
    }

    private void SetupNewBrick()
    {
        boardModel.brickViewModel = nextBrickModel.GetNextBrick();
        nextBrickModel.ChooseNextBrick();
        nextBrickBoardView.DrawNextBrick();
        boardModel.SetUpStartPosition();
        boardView.DrawBoardState(boardModel);
    }

    private IEnumerable CheckFullLines()
    {
        List<int> indxOfFullLines = boardModel.GetFullLinesIndx();
        if (indxOfFullLines.Count != 0)
        {
            boardModel.DeleteFullLines(indxOfFullLines);
            boardView.DrawBoardState(boardModel);
            yield return new WaitForSeconds(fullLinesPauseTime);
            //czekaj 0.5 sekundy
            boardModel.MovePartsAfterDeleting(indxOfFullLines);
            boardView.DrawBoardState(boardModel);
            boardModel.AddToScore(indxOfFullLines.Count);
            if (boardModel.GetScore >= boardModel.NextLevelThreshold)
            {
                boardModel.IncreaseLevel();
                moveTime -= 0.2f;
            }

            boardView.UpdateTextInfo(boardModel);
        }
    }
    
    private IEnumerable MoveNewBrickDown()
    {
        if(boardModel.IsPositionValid(boardModel.brickViewModel.Bitmap, new KeyValuePair<int, int>(boardModel.currentBrickPosition.Key + 1, boardModel.currentBrickPosition.Value)) == false && 
            boardModel.previousBrickPosition.Key == boardModel.currentBrickPosition.Key
            && boardModel.previousBrickPosition.Value == boardModel.currentBrickPosition.Value)
        {
            if (boardModel.currentBrickPosition.Key ==  boardModel.startBrickPosition.Key && boardModel.currentBrickPosition.Value ==  boardModel.startBrickPosition.Value)
            {
                isGameOver = true;
                boardView.ShowGameOverText();
            }
            //Debug.Log("Zapisuje");
            boardModel.SaveBrickToBoard();
            boardView.DrawBoardState(boardModel);
            //boardView.AddBrickToBoard(boardModel);
            foreach (var e in CheckFullLines())
            {
                yield return e;
            }
            SetupNewBrick();
        }
        boardModel.MoveNewBrick(new KeyValuePair<int, int>(1,0));
        boardView.DrawNewBrick(boardModel);
    }

    public void Awake()
    {
        keySystem = new KeySystem();
        nextBrickModel = new NextBrickBoardViewModel();
        nextBrickBoardView.Setup(nextBrickModel);
        nextBrickBoardView.DrawNextBrick();
    }

    private IEnumerable MainLoop()
    {
        while (isGameOver == false)
        {
            foreach (var e in MoveNewBrickDown())
            {
                yield return e;
            }
            float previousTime = Time.time;
            while (previousTime + moveTime >= Time.time)
            {
                keySystem.Update();
                yield return null;
                CheckKeyboard(out var wasDownPressed);
                if (wasDownPressed) break;
            }
        }
        Debug.Log("GAME OVER");
    }
    void Start()
    {
        boardModel = new MainBoardViewModel(25, 16);
        boardView = GetComponent<MainBoardView>();
        boardView.UpdateTextInfo(boardModel);
        boardView.InstantiateBrickParts(boardModel);
        boardView.SetBoardSize(boardModel);
        SetupNewBrick();
        StartCoroutine(MainLoop().GetEnumerator());
    }
}
