﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNextBrickChooser : NextBrickChooser
{
    public BrickViewModel ChooseNextBrick()
    {
        BrickModel brickModel = new BrickModel(Random.Range(0, BrickList.BrickNames.Length));
        Color color = new Color(
            Random.Range(0f, 1f), 
            Random.Range(0f, 1f), 
            Random.Range(0f, 1f)
        );
        BrickViewModel brickViewModel= new BrickViewModel(brickModel.bitmap, color);
        return brickViewModel;
    }
}
