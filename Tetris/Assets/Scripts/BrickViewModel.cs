﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickViewModel
{
    public bool[,] Bitmap { get; private set; }
    public Color BrickColor{ get; set; }
    public bool IsFieldFilled(int i, int j) => Bitmap[i,j];
    public int BitmapHeight=> Bitmap.GetLength(0);
    public int BitmapWidth => Bitmap.GetLength(1);

    public void RotateBrick()
    {
        bool[,] newBitmap = new bool[BitmapWidth, BitmapHeight];
        for (int i = 0; i < BitmapHeight; i++)
        {
            for (int j = 0; j < BitmapWidth; j++)
            {
                newBitmap[BitmapWidth - j-1,i] = Bitmap[i,j];
            }
        }

        Bitmap = newBitmap;
    }
    public bool[,] GetRotatedBitmap()
    {
        bool[,] newBitmap = new bool[BitmapWidth, BitmapHeight];
        for (int i = 0; i < BitmapHeight; i++)
        {
            for (int j = 0; j < BitmapWidth; j++)
            {
                newBitmap[BitmapWidth - j-1,i] = Bitmap[i,j];
            }
        }

        return newBitmap;
    }

    public BrickViewModel(bool[,] bitmap, Color color)
    {
        Bitmap = bitmap;
        BrickColor = color;
    }

}
