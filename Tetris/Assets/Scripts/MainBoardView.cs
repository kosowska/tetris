﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainBoardView : MonoBehaviour
{
    [SerializeField] 
    private GameObject board;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI lvlText;
    [SerializeField] private TextMeshProUGUI gameOverText;
    private BrickDrawer brickDrawer;
    private Dictionary<Tuple<int, int>, GameObject> brickParts = new Dictionary<Tuple<int, int>, GameObject>();
    private GameObject newBrick;
    void Awake()
    {
        brickDrawer = GetComponent<BrickDrawer>();
        gameOverText.gameObject.SetActive(false);
    }
    public void DrawNewBrick(MainBoardViewModel boardModel)
    {
        Vector3 startPosition = board.transform.position + new Vector3(boardModel.currentBrickPosition.Value * brickDrawer.oneUnit, -boardModel.currentBrickPosition.Key * brickDrawer.oneUnit, 0);
        brickDrawer.DrawNewBrick(boardModel.brickViewModel, startPosition);
    }

    public void UpdateTextInfo(MainBoardViewModel boardModel)
    {
        scoreText.text = $"Points: {boardModel.GetScore}";
        lvlText.text = $"Level: {boardModel.GetLevel}";
    }

    public void ShowGameOverText()
    {
        gameOverText.gameObject.SetActive(true);
    }

    /*public void AddBrickToBoard(MainBoardViewModel boardModel)
    {
        Vector3 startPosition = board.transform.position + new Vector3(boardModel.currentBrickPosition.Value * brickDrawer.oneUnit, -boardModel.currentBrickPosition.Key * brickDrawer.oneUnit, 0);

        //Vector3 currentBoardPos = startPosition;
        for (int i = 0; i < boardModel.brickViewModel.BitmapHeight; i++)
        {
            for (int j = 0; j < boardModel.brickViewModel.BitmapWidth; j++)
            {
                Tuple<int,int> position = new Tuple<int, int>(i+boardModel.currentBrickPosition.Key, j+boardModel.currentBrickPosition.Value);
                if (position.Item1 < boardModel.BoardHeight && position.Item2 < boardModel.BoardWidth && 
                    boardModel.brickViewModel.IsFieldFilled(i, j))
                {
                    brickParts[position].SetActive(true);
                    brickParts[position].GetComponent<SpriteRenderer>().color = boardModel.brickViewModel.BrickColor;
                }
                //currentBoardPos.x += brickDrawer.oneUnit;
            }
            //currentBoardPos.y -= brickDrawer.oneUnit;
            //currentBoardPos.x = board.transform.position.x;
        }
    }*/
    public void DrawBoardState(MainBoardViewModel boardModel)
    {
        Vector3 currentBoardPos = board.transform.position;
        for (int i = 0; i < boardModel.BoardHeight; i++)
        {
            for (int j = 0; j < boardModel.BoardWidth; j++)
            {
                Tuple<int,int> position = new Tuple<int, int>(i, j);
                if (boardModel.IsFieldFilled(i, j))
                {
                    brickParts[position].SetActive(true);
                    brickParts[position].GetComponent<SpriteRenderer>().color = boardModel.GetFieldColor(i, j);
                }
                else
                {
                    brickParts[position].SetActive(false);
                }
                currentBoardPos.x += brickDrawer.oneUnit;
            }
            currentBoardPos.y -= brickDrawer.oneUnit;
            currentBoardPos.x = board.transform.position.x;
        }
    }
    
    public void InstantiateBrickParts(MainBoardViewModel boardModel)
    {
        Vector3 currentBoardPos = board.transform.position;
        for (int i = 0; i < boardModel.BoardHeight; i++)
        {
            for (int j = 0; j < boardModel.BoardWidth; j++)
            {
                Tuple<int,int> pozycja = new Tuple<int, int>(i, j);
                brickParts[pozycja] = brickDrawer.DrawBrickPart(currentBoardPos);
                currentBoardPos.x += brickDrawer.oneUnit;
            }
            currentBoardPos.y -= brickDrawer.oneUnit;
            currentBoardPos.x = board.transform.position.x;
        }
    }
    
    public void SetBoardSize(MainBoardViewModel boardModel)
    {
        board.GetComponent<SpriteRenderer>().size = 
            new Vector3(boardModel.BoardWidth * brickDrawer.oneUnit, boardModel.BoardHeight * brickDrawer.oneUnit, 1f);
    }
}
