﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextBrickBoardView : MonoBehaviour
{
    [SerializeField] 
    private SpriteRenderer boardOutline;
    [SerializeField] 
    private Transform placeForBrick;
    private BrickDrawer brickDrawer;
    private NextBrickBoardViewModel nextBrickModel;
    public void SetBoardSize()
    {
        var rectTransform = GetComponent<RectTransform>();
        boardOutline.size = new Vector3(rectTransform.rect.height, rectTransform.rect.width, 1f);
    }
    public void DrawNextBrick()
    {
        brickDrawer.DrawNewBrick(nextBrickModel.GetNextBrick(), placeForBrick.position);
    }

    public void Setup(NextBrickBoardViewModel nextBrickBoardViewModel)
    {
        nextBrickModel = nextBrickBoardViewModel;
    }
    public void Awake()
    {
        brickDrawer = GetComponent<BrickDrawer>();
        SetBoardSize();
    }
}
