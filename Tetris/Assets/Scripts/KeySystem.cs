using System.Collections.Generic;
using UnityEngine;

public class KeySystem
{
    private KeyCode[] keyCodes = new[] {KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.UpArrow, KeyCode.DownArrow};
    private Dictionary<KeyCode, float> nextKeyEventTime = new Dictionary<KeyCode, float>();
    private HashSet<KeyCode> pressedKeys = new HashSet<KeyCode>();
    private float t0 = 0.5f;
    private float t1 = 0.1f;
    public void Update()
    {
        pressedKeys.Clear();
        foreach (var keyCode in keyCodes)
        {
            if (Input.GetKeyDown(keyCode))
            {
                nextKeyEventTime[keyCode] = Time.time + t0;
                pressedKeys.Add(keyCode);
            }
            else if (Input.GetKeyUp(keyCode))   
            {
                pressedKeys.Remove(keyCode);
                nextKeyEventTime.Remove(keyCode);
            }

            else if (Input.GetKey(keyCode) && nextKeyEventTime.ContainsKey(keyCode))
            {
                if (Time.time >= nextKeyEventTime[keyCode])
                {
                    nextKeyEventTime[keyCode] += t1;
                    pressedKeys.Add(keyCode);
                }
            }
        }
    }
    public bool GetKeyDown(KeyCode keyCode) => pressedKeys.Contains(keyCode);
}