﻿
public class NextBrickBoardViewModel 
{
    private BrickViewModel nextBrick;
    private readonly NextBrickChooser _nextBrickChooser;

    public NextBrickBoardViewModel()
    {
        _nextBrickChooser = new RandomNextBrickChooser();
        ChooseNextBrick();
    }
    public BrickViewModel GetNextBrick()
    {
        if (nextBrick == null)
        {
            ChooseNextBrick();
        }
        return nextBrick;
    }

    public void ChooseNextBrick()
    {
        nextBrick = _nextBrickChooser.ChooseNextBrick();
    }
}
