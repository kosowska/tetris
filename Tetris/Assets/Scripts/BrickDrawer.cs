﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickDrawer : MonoBehaviour
{
    public float oneUnit;
    [SerializeField]
    private GameObject brickPart;
    private Dictionary<Tuple<int, int>, GameObject> brickParts = new Dictionary<Tuple<int, int>, GameObject>();

    private void Awake()
    {
        oneUnit = brickPart.GetComponent<RectTransform>().rect.width;
    }

    public GameObject DrawBrickPart(Vector3 position)
    {
        return Instantiate(brickPart, position, Quaternion.identity);
    }
    public void DrawNewBrick(BrickViewModel brickModel, Vector3 startPosition)
    {
        ClearNewBrickParts();
        Vector3 currentPos = startPosition;
        for (int i = 0; i < brickModel.BitmapHeight; i++)
        {
            for (int j = 0; j < brickModel.BitmapWidth; j++)
            {
                Tuple<int,int> position = new Tuple<int, int>(i, j);
                if(brickParts.ContainsKey(position) == false){
                    GameObject newBrickPart = DrawBrickPart(currentPos);
                    brickParts[position] = newBrickPart;
                }

                brickParts[position].transform.position = currentPos;
                if (brickModel.IsFieldFilled(i, j))
                {
                    brickParts[position].SetActive(true);
                    brickParts[position].GetComponent<SpriteRenderer>().color = brickModel.BrickColor;
                }
                else
                {
                    brickParts[position].SetActive(false);
                }
                currentPos.x += oneUnit;
            }
            currentPos.y -= oneUnit;
            currentPos.x = startPosition.x;
        }
    }
    private void ClearNewBrickParts()
    {
        foreach (var part in brickParts)
        {
            part.Value.SetActive(false);
        }    
    }
}
